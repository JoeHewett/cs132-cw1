#include <stdio.h>

// Factorial takes a number n and returns the factorial of that number
long long factorial(n) {
    long long int fact = 1;
    int i = 0;
    
    for (i = 1; i <= n; ++i) {
        fact *= i;
    }
    return fact;
}

// This function takes the n and k and substitutes them into an equation
// that utilises the factorial function. The equation is similar to the C 
// function used to find binomial coefficients
long long pascal(int n, int k) {
    long long int result = 0;
    result = factorial(n) / (factorial(n-k) * factorial(k));
    return result; 
}

// In main we simply sanitise inputs and reject incorrect values, then call 
// the Pascal function with our given n and k, and print the result to console
int main(int argc, char *argv[]) {
    if (argc != 2) { 
        printf("Incorrect number of arguments. Usage: ./pascal <int>\n");
        return 0;
    }
    int i = 0;
    int n = atoi(argv[1]);
    long long int result = 0;

    if (n < 0)  {
        printf("One or more of your inputs is not a positive integer.\n");
    } else if(n > 20) {
        printf("The program is limited to finding the 20th row of Pascals Triangle.\n");
    } else {
        for (i = 0; i <= n; i++) {
            result = pascal(n, i);
            printf("%d  ", result);
        }
    }
    printf("\n");    
    // printf("The value %d of the row %d is: %lld\n", k, n, result); 
    return 0;
}


