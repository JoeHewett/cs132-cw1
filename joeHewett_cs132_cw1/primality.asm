prime: ds.b 1
number: ds.b 2
divisor: ds.b 2
half: ds.b 2

move.b #21, D0
move.b D0, number
move.b #2, D0
move.b D0, divisor

| perform division by 2 to get 
| a value for "half". If last 
| sub = 0 we know 
MOVE.b number, D2
MOVE.b #0, D0
gethalf: INC D0
SUB #2, D2
BEQ notprime 
BPL gethalf
MOVE.b D0, half

| prog comes here to inc
| divisor after each divisor checked.
| If divisor >= half then branch to isprime
nextdivisor: MOVE.b divisor, D0
INC D0
MOVE.b half, D1
SUB D0, D1
BLE isprime
MOVE.b D0, divisor 

MOVE.b number, D2
MOVE.b divisor, D3

| main sub loop. Keep subbing divisor until
| remainder =< 0. 3 branches depending on 
| if remainder < 0, 0 or > 0. 
loop: SUB D3, D2
BEQ notprime
BPL loop
BMI nextdivisor:

| if remainder ever 0 branch here as 
| we have found a divisor. Set prime=0
notprime: MOVE.b #0, D0
MOVE.b D0, prime
JMP end

| if every divisor up to half checked then 
| we branch here as num = prime. Set prime=1
isprime: MOVE.b #1, D0
MOVE.b D0, prime
JMP end

end: MOVE.b prime, D2


