#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int* getTwosComp(int decimalTotal) {
    int absoluteTotal = abs(decimalTotal);
    static int oneC[9];
    static int twoC[9];
    int carryBit = 1;
    int bit; 
    int i;

    int* pointer;
 
    // If positive we populate buffer with 0
    // If negative we populate buffer with 1   
    for(i = 0; i < 8; i++) {
        if(decimalTotal < 0)
            oneC[i] = 1;
        else
            oneC[i] = 0;
    }
    
    // While abs val of our num > 0, loop through dividing by 2 
    // each time and performing modulo 2 of current absoluteTotal
    // If out total > 0 and mod 2 = 1 (num odd) then set bit to 0
    // else if mod 2 = 0 (num even) set bit 1. Reverse for total < 0
    for(i = 0; absoluteTotal > 0; i++) {
        if(decimalTotal < 0) {
            if(absoluteTotal % 2 == 1)
                bit = 0;
            else 
                bit = 1;
        } else {
            if(absoluteTotal % 2 == 1) 
                bit = 1;
            else
                bit = 0;
        }

        // Set the index (specified by current incrementor) of ones 
        // complement buffer to the bit we just worked out
        oneC[i] = bit;
        absoluteTotal = absoluteTotal / 2;
    }
    
    // If our number is > 0 we can return ones complement
    // If our number < 0 we must add 1 (2c = flip bits of 1c and add 1)
    if(decimalTotal > 0) 
        pointer = oneC;
    else {
        // Basic binary arithmetic here. Carry is preset to 1 when declared
        // If 1 & 1, 2c = 0, if 0, 1 then 2c = 1. 
        for(i = 0; i < 8; i++) {
            if(oneC[i] == 1 && carryBit == 1) {
                twoC[i] = 0;
            } else if(oneC[i] == 0 && carryBit == 1) {
                twoC[i] = 1;
                carryBit = 0;
            } else {
                // When val has been added, leave all other values the same
                twoC[i] = oneC[i];
            }
            
        }
        pointer = twoC;       
    }
    return pointer; 
}


int main() {
    char bin1[9];
    char bin2[9];

    int i,c;
    int power = 0;
    int signBit = 0;
    
    int num1 = 0;
    int num2 = 0; 
    int decimalTotal = 0;
    int* twosCompTotal;

    // Take two user inputs limited to 8 chars
    printf("Enter your first binary number: ");
    scanf("%8s", bin1);

    printf("Enter your second binary number: ");
    scanf("%8s", bin2);

    // Loops from last digit to second digit, ensuring bits = 0 or 1 and
    // passes place value and bit to getBitValue, where value is calculated
    for (c = 7; c > 0; c--) {
        if((bin1[c] == '1' || bin1[c] == '0') && (bin2[c] == '1' || bin2[c] == '0')) {
                num1 += getBitValue(power, bin1[c]);
                num2 += getBitValue(power, bin2[c]);
                power++;
        } else {
            printf("\nERROR - Input contains an illegal character or is wrong length\n");
            printf("Ensure both inputs are 8 bit binary numbers.\n");
            return 0;
        }
    }
 
    // Check the sign bits are valid for both number then apply sign bit   
    // This is where the sum of the two values takes place
    if((bin1[0] - '0' == 1 || bin1[0] - '0' == 0) && (bin2[0] - '0' == 1 || bin2[0] - '0' == 0) ) {
        num1 = applySign(num1, bin1[0]);
        num2 = applySign(num2, bin2[0]);
        printf("Binary Num 1 is %d\n", num1);
        printf("Binary Num 2 is %d\n", num2);
    
        decimalTotal = num1 + num2;
    } else { 
        printf("\nERROR - Input contains an illegal character or is wrong length\n");
        printf("Ensure both inputs are 8 bit binary numbers.\n");
        return 0;
    }
 
    // If summation will cause overflow/underflow then reject inputs 
    // Else we will have a 2c output. If sum = 0 then return 0, else call 
    // getTwosComp to calculate value in 2c   
    if(decimalTotal > 128 || decimalTotal < -127) {
        printf("Buffer overflow/underflow exception. Please use smaller numbers.");
    } else {
        printf("Twos Complement Result = ");
        if(decimalTotal == 0) {
            printf("00000000");
        } else {
            twosCompTotal = getTwosComp(decimalTotal); 
            for(i = 7; i >= 0; i--) {
                printf("%d", *(twosCompTotal + i));
            }
        }
    }

    printf("\n");
    return 0;
    
}

// Given a 0 or 1 and a power, determine the value of a bit in a binary number
int getBitValue(int power, int bit) {
    int value = 0;
    int placeValue = pow(2, power);
    int convertedBit = bit - '0';

    value = convertedBit * placeValue;
    return value;
}

// Takes a value and the provided sign bit and makes val -ve if sign = 1
int applySign(int value, int sign) {
    int signBit = sign - '0';
    int newValue = 0;

    if (signBit == 1)
       newValue -= value;
    else if (signBit == 0)
        newValue = value;
    return newValue;   
}
