#include <stdio.h>

int main(int argc, char *argv[])  
{ 
  
    // Checking if number of args is 2 (file name and numbers to find GCD for)
    if (argc != 3) { 
        printf("You must provide 2 arguments - usage: ./gcd <int> <int>\n"); 
        return 0; 
    } 
 
    int a, b; 
     
    // Converting string type to integer type using atoi(string) 
    a = atoi(argv[1]);  
    b = atoi(argv[2]);

    if (a < 1 || b < 1) {
        printf("You have provided an invalid argument. Please provide 2 positive integers.\n");
        return 0;
    }

    // Euclids GCD algorithm starts here
    //Keep subtracting a from b and b from a while one is greater than the other
    // Until b = 0, at which point the value in a is the GCD
    while(b != 0) {
        
        if (a > b) {
            printf ("%d (a) is bigger than %i (b) so subtracting b from a\n", a, b);
            a = a - b;
        } else if (b > a) {
            printf ("%d (b) is bigger than %i (a) so subtracting a from b\n", b, a);
            b = b - a;
        } else { 
            printf ("%d is the greatest common divisor\n", a);
            b = b - a;
        }
    }
    
    return 0; 
} 







