|sum = 0;
|for (i = n; i != 0, i--) {
|    sum = sum +i;
|}


n: ds.b 1
counter: ds.b 1
total: ds.b 2

move.b #10, D1
move.b D1, n

move.b #1, D1
move.b D1, counter

move.b #0, D1
move.b D1, total

start: move.b n, D0
move.b counter, D1
sub D1, D0
bmi end:
move.b total, D3
add D1, D3
move.b D3, total
inc D1
move.b D1, counter
jmp start
end: move.b D0, D0
