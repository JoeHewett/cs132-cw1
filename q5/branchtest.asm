bignum: ds.b 1
smallnum: ds.b 1

move.b #10, D1
move.b D1, bignum

move.b #1, D2
move.b D2, smallnum

move.b smallnum, D2
move.b bignum, D1

jmp loop
infinite: jmp infinite

loop: sub D1, D2
bmi infinite:

end: move.b D1, D1
