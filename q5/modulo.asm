| DECLARE AND SET VALUES
remainder: ds.b 1
divisor: dc.b #4
MOVE.b #15, D0
MOVE.b D0, remainder

| PUT VARIABLES INTO REGISTERS
MOVE.b remainder, D2
MOVE.b divisor, D3

| SUB DIVISOR FROM REMAINDER
| UNTIL RESULT LESS/EQ TO 0
| IF LESS, DO NOT UPDATE REMAINDER
start: SUB D3, D2
BMI end
MOVE.b D2, remainder
BEQ end
JMP start

| PUT REMAINDER INTO D3
end: MOVE.b remainder, D3


